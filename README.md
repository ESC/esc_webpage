EIC Software Consortium (ESC)
=============================

Introduction
------------

### Objectives

### Near-term Goals

References and Links
--------------------

[Software tools](software_tools.md)

[ESC Meetings]

### Meetings

* [July 2016 EIC User Group Meeting at Argonne][2]
* [EIC Generic Detector R&D Meeting][5]
* [January 2016 EIC User Group Meeting at Berkeley][4]

* [BNL wiki with eRHIC Simulations][1]
* [HepSim EIC ep][3] 

[1]: https://wiki.bnl.gov/eic/index.php/Simulations
[2]: http://eic2016.phy.anl.gov
[3]: https://en.support.wordpress.com/markdown-quick-reference/
[4]: http://portal.nersc.gov/project/star/jthaeder/eicug2016/index.php?id=0
[5]: https://wiki.bnl.gov/conferences/index.php/July_2016


[Markdown quick 
reference](https://en.support.wordpress.com/markdown-quick-reference/)
