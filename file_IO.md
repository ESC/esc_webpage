
LCIO
----

Origins begin with SIO (1999). Used as the serialization back-end. LCIO (2003) 
defines a standard set of objects for analysis.
Has bindings for reading 

LCIO Claims:
1. Faster reading
2. Smaller file size

These of claims course a void of context.  Looking at the implementation of the 
event:

```cpp
  protected:
     int _runNumber ;
    int _eventNumber ;
    EVENT::long64 _timeStamp ;
    std::string _detectorName ;
    
    // map has to be defined mutable in order to use _map[]  for const methods ...
    mutable LCCollectionMap _colMap ;
    mutable std::vector<std::string> _colNames ;
    
    LCParametersImpl _params ;
    
    // set of collections that are not owned by the event anymore
    mutable LCCollectionSet _notOwned ;
```
